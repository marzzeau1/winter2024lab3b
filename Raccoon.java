public class Raccoon{
	public String name;
	public String colour;
	public int numberOfStripesOnTail;
	
	public void doHypnosis(){
		System.out.println("THE RACCOON HYPNOTIZES YOU WITH HIS " + this.numberOfStripesOnTail + " STRIPES ON HIS TAIL! YOU ARE LEFT CONFUSED AND HYPNOTIZED." + "you can only remember one word from the incident: " + this.name + " ...");
	}
	public void doRobbery(){
		System.out.println("THE " + this.colour + " RACCOON CAMOUFLAGES HIMSELF WITH THE " + this.colour + " WALLS AND SURPRISES YOU WITH HIS CLAWS, HE TAKES ALL OF YOUR MARSHMELLOWS! It says: YOU WERE ROBBED BY THE GREAT " + this.name + ". You are left dazed and confused.");
	}
}