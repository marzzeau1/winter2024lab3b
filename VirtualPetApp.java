import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Raccoon[] smackOfRaccoons = new Raccoon[5];
		
		for(int i = 0; i < smackOfRaccoons.length; i++){
			smackOfRaccoons[i] = new Raccoon();
			
			System.out.println("What shall raccoon number:" + i + " be named?");
			smackOfRaccoons[i].name = reader.nextLine();
			
			System.out.print("What colour shall raccoon number: " + i + " bear\r\n");
			smackOfRaccoons[i].colour = reader.nextLine();
			
			System.out.print("How many stripes will raccoon number: " + i + " have on his tail\r\n");
			smackOfRaccoons[i].numberOfStripesOnTail = Integer.parseInt(reader.nextLine());
		}
		
		System.out.println(smackOfRaccoons[4].name);
		System.out.println(smackOfRaccoons[4].colour);
		System.out.println(smackOfRaccoons[4].numberOfStripesOnTail);
		
		smackOfRaccoons[0].doHypnosis();
		smackOfRaccoons[0].doRobbery();
	}
}